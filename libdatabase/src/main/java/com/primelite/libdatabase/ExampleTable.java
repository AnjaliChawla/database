package com.primelite.libdatabase;

/**
 * Created by Anjali Chawla on 28/9/17.
 */

public class ExampleTable {
    String name;
    int id;
    String note;
    int status;
    String created_at;

    // constructors
    public ExampleTable() {
    }

    // setters

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public void setCreatedAt(String created_at) {
        this.created_at = created_at;
    }

    // getters
    public long getId() {
        return this.id;
    }

    public String getNote() {
        return this.note;
    }

    public int getStatus() {
        return this.status;
    }
}