package com.primelite.libdatabase;

import android.content.Context;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import static android.R.attr.version;

/**
 * Created by Anjali Chawla on 28/9/17.
 */

public class DatabaseHelper extends SQLiteOpenHelper {

    private static DatabaseHelper mDatabseInstance;
    private Context context;

    private static int DATABASE_VERSION = 1;

    public DatabaseHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
        this.DATABASE_VERSION = version;
        this.context = context;
    }

    public DatabaseHelper(Context context, String name, SQLiteDatabase.CursorFactory factory,
                          int version, DatabaseErrorHandler errorHandler) {
        super(context, name, factory, version, errorHandler);
        this.DATABASE_VERSION = version;
        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        try {
            int version = db.getVersion();
            version = version > this.DATABASE_VERSION ? version : this.DATABASE_VERSION;
            List<Map<String, List<String>>> dbQuery = parsed(0, version, true);
            up(db, dbQuery);

        } catch (Exception e) {
            System.out.println("eve : Database creation throw some exception" + e.getMessage());
        }
    }

    private void up(SQLiteDatabase db, List<Map<String, List<String>>> dbQuery) {
    }

    private List<Map<String, List<String>>> parsed(int i, int version, boolean b) {
        List<Map<String, List<String>>> dbQuery =
        return
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public static synchronized DatabaseHelper getDatabaseInstance(Context context) {
        if (mDatabseInstance == null) {
            mDatabseInstance = new DatabaseHelper(context.getApplicationContext(), "", null, 1);
        }
        return mDatabseInstance;
    }

}
