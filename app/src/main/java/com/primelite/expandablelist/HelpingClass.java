package com.primelite.expandablelist;

import android.widget.ExpandableListView;

/**
 * Created by Anjali Chawla on 28/9/17.
 */

public class HelpingClass implements ExpandableListView.OnGroupExpandListener {
    private int lastExpandedPosition = -1;
    private CustExpListview secondLevelexplv;

    public HelpingClass(CustExpListview secondLevelexplv) {
        this.secondLevelexplv = secondLevelexplv;
    }

    @Override
    public void onGroupExpand(int intGroupPosition) {
        System.out.println("here :onGroupExpand");
        if (lastExpandedPosition != -1
                && intGroupPosition != lastExpandedPosition) {
            secondLevelexplv.collapseGroup(lastExpandedPosition);
        }
        lastExpandedPosition = intGroupPosition;
    }
}

