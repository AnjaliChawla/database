package com.primelite.expandablelist;

import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

/**
 * Created by Anjali Chawla on 27/9/17.
 */

public class ParentLevel extends BaseExpandableListAdapter {
    private Context context;

    public ParentLevel(MainActivity mainActivity) {
        this.context = mainActivity;
    }

    @Override
    public int getGroupCount() {
        return 10;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return 5;
    }

    @Override
    public Object getGroup(int groupPosition) {
        return groupPosition;
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {

        TextView tv = new TextView(context);
        tv.setText("->FirstLevel");
        tv.setBackgroundColor(Color.CYAN);
        tv.setHeight(150);
        tv.setPadding(10, 7, 27, 7);

        return tv;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        final CustExpListview SecondLevelexplv = new CustExpListview(context);
        SecondLevelexplv.setAdapter(new SecondLevelAdapter(context));
        SecondLevelexplv.intChildPosition = childPosition;
        SecondLevelexplv.intGroupPosition=groupPosition;
        SecondLevelexplv.setOnGroupExpandListener(new HelpingClass(SecondLevelexplv));
        SecondLevelexplv.setGroupIndicator(null);
        return SecondLevelexplv;

    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}
